/* 
	Problem 3: Write a function that will return all cards that belong to a 
    particular list based on the listID that is passed to it from the given data
    in cards.json. Then pass control back to the code that called it by using a 
    callback function.
*/

const fs = require('fs');
const path = require('path');

let absPath = path.resolve(__dirname,'./cards.json');

function allCardsBelongToList(listID, callBack) {
    setTimeout(() => {
        fs.readFile(absPath, 'utf-8', (error, data) => {
            if (error) {
                callBack(new Error("file not found"));
            }
            else {
                const cardListData = JSON.parse(data);
                const  card = Object.keys(cardListData).filter((id) => {
                    return id === listID;
                });
                callBack(null, cardListData[card]);
            }
        });
    }, 2 * 1000);
}


 module.exports = allCardsBelongToList;