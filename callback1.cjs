/* 
    Problem 1: Write a function that will return a particular board's information 
    based on the boardID that is passed from the given list of boards in boards.json
     and then pass control back to the code that called it by using a callback function.
*/

const fs = require('fs');
const path = require('path');

let absPath = path.resolve(__dirname,'./boards.json');

function boardsInformation(boardID, callBack) {
    setTimeout(() => {
        fs.readFile(absPath, 'utf-8', (error, data) => {
            if (error) {
                callBack(new Error("file not found"));
            }
            else {
                const boardsData = JSON.parse(data);
                const boardsInfo = boardsData.filter((board) => {
                    return board.id === boardID;
                });
                callBack(null, boardsInfo);
            }
        });
    }, 2 * 1000);
}

module.exports = boardsInformation;