/* 
    Problem 2: Write a function that will return all lists 
    that belong to a board based on the boardID that is passed to it 
    from the given data in lists.json. 
    Then pass control back to the code that called it by using a callback function.
*/
const fs = require('fs');
const path = require('path');

let absPath = path.resolve(__dirname,'./lists.json');

function allListBelongToBoard(boardID, callBack) {
    setTimeout(() => {
        fs.readFile(absPath, 'utf-8', (error, data) => {
            if (error) {
                callBack(new Error("file not found"));
            }
            else {
                const boardsListData = JSON.parse(data);
                const  board = Object.keys(boardsListData).filter((id) => {
                    return id === boardID;
                });
                callBack(null, boardsListData[board]);
            }
        });
    }, 2 * 1000);
}

module.exports = allListBelongToBoard;