/* 
    Problem 4: Write a function that will use the previously written functions 
    to get the following information. You do not need to pass control back to the 
    code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/


const fs = require('fs');
const path = require('path');
const boardsInformation = require('./callback1.cjs');
const allListBelongToBoard = require('./callback2.cjs');
const allCardsBelongToList = require('./callback3.cjs');



let absPath = path.resolve(__dirname,'./boards.json');

function thanosBoardsInformationWithMind(boardName) {
    setTimeout(() => {

        fs.readFile(absPath, 'utf-8', (error, data) => {
            if (error) {
                console.error(new Error("file not found"));
            }
            else {
                const boardsData = JSON.parse(data);
                const boardsInfo = boardsData.filter((board) => {
                    return board.name == boardName;
                });
                let boardId = boardsInfo[0].id;
                //Get information from the Thanos boards
                boardsInformation(boardId, (error, thanosData) => {
                    if (error) {
                        console.error(error);
                    }
                    else {
                        console.log("Information from the Thanos boards");
                        console.log(thanosData);

                        //Get all the lists for the Thanos board
                        allListBelongToBoard(boardId, (error, thanosListData) => {
                            if (error) {
                                console.error(error);
                            }
                            else {
                                console.log("All the lists for the Thanos board");
                                console.log(thanosListData);

                                //Get all cards for the Mind list simultaneously
                                let mindInfo = thanosListData.find((listData) => {
                                    return listData.name === "Mind";
                                });
                                let mindId = mindInfo.id;
                                allCardsBelongToList(mindId, (error, mindInfo) => {
                                    if (error) {
                                        console.error(error);
                                    }
                                    else {
                                        console.log('All cards for the Mind list simultaneously');
                                        mindInfo.map((data) => {
                                            return data;
                                        });
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    }
                });

            }
        });
    }, 2 * 1000);
}

module.exports = thanosBoardsInformationWithMind;