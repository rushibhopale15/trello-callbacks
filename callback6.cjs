/* 
	Problem 6: Write a function that will use the previously written functions to get the 
    following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/
const fs = require('fs');
const path = require('path');
const boardsInformation = require('./callback1.cjs');
const allListBelongToBoard = require('./callback2.cjs');
const allCardsBelongToList = require('./callback3.cjs');


let absPath = path.resolve(__dirname,'./boards.json');

function thanosBoardsInformationWithAllCards(boardName) {
    setTimeout(() => {

        fs.readFile(absPath, 'utf-8', (error, data) => {
            if (error) {
                console.error(new Error("file not found"));
            }
            else {
                const boardsData = JSON.parse(data);
                const boardsInfo = boardsData.filter((board) => {
                    return board.name == boardName;
                });
                let boardId = boardsInfo[0].id;
                //Get information from the Thanos boards
                boardsInformation(boardId, (error, thanosData) => {
                    if (error) {
                        console.error(error);
                    }
                    else {
                        console.log("Information from the Thanos boards");
                        console.log(thanosData);

                        //Get all the lists for the Thanos board
                        allListBelongToBoard(boardId, (error, thanosListData) => {
                            if (error) {
                                console.error(error);
                            }
                            else {
                                console.log("All the lists for the Thanos board");
                                console.log(thanosListData);

                                //Get all cards for all lists simultaneously
                                let allListData = thanosListData.map((listData)=>{
                                    //console.log(listData.id);
                                    allCardsBelongToList(listData.id, (error,currData)=>{
                                        if (error) {
                                            console.error(error);
                                        }
                                        else {
                                            //console.log(currData);
                                            if(currData !== undefined){
                                            let listData = currData.map((data)=>{
                                                return data;
                                            })
                                            console.log(listData);
                                        }
                                        }
                                        return currData;
                                    })
                                })
                            }
                        });
                    }
                });

            }
        });
    }, 2 * 1000);
}

module.exports = thanosBoardsInformationWithAllCards;